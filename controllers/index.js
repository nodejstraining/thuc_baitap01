
var express = require('express');
var router = express.Router(); 
var models = require('../models/schema');
var mongoose = require("mongoose");
 mongoose.Promise = global.Promise;
 
 router.route('/')
  .get(function(req, res){
    res.render('index')
  });

//route xu ly phan login
 router.route('/login')
  .post(function(req, res){
     
     models.User.findOne({name: req.body.name,  password: req.body.password},function(err, user){
        if(err){
          console.log("can not find name and password");
          res.send("bi loi khi dang nhap");
        }else if(user){
          req.session.user = user.name;

          res.redirect('home');
          console.log("Dang nhap thanh cong \n"+user.name);
        }else{
          
          res.redirect('/');
          console.log("invalid");
        }
     });
    
  });

//route xu li phan register
 router.route('/register').get(function(req, res){
    res.render('register');
 });
 router.route('/Signup').post(function(req, res){
    models.User({
      name:     req.body.name, 
      email:    req.body.email,
      password: req.body.password

    }).save(function(error){
      if(error){
        console.log("error to insert user");
        res.redirect('Signup');
      }else{
        res.redirect('/');
      }
    })
 });
//route xu ly trang home
//put data to client 
router.route('/home').get(function(req, res){
  if(req.session.user == null){
     res.redirect("/");
   }else{

    models.Student.find({}, function(err, result){

      if(err){
        console.log(err);
        res.send("can't find document");
      }else if(result.length){

        // console.log('Found: ', result);
        console.log("List student");
        res.render('home', {result_Inf:result, session_name: req.session.user});
      }else{
        console.log("No document");
        res.render('home', {'result_Inf': false});
      }
    });
   }
 
});
//lay gia tri tu form insert student
router.route('/insertStd').post(function(req, res){
  var gt;
  if(req.body.gender=='nam'){
    gt = true;
  }else {
    gt = false;
  }

  models.Student({
    username: req.body.username, 
    email: req.body.email, 
    address: req.body.address, 
    gender: gt
  }).save(function(error){
    if(error){
      console.log("Error to insert student");
      res.send("Error to insert student");
    }else{
      res.redirect('home');
    }
  });
});

//Router xu ly sua 
router.route('/editStd').post(function(req, res){
  var username_last = req.body.username_edit;
  var username_first = req.body.username_edit_hidden;
  var email = req.body.email_edit;
  var address = req.body.address_edit;
  var gender;
  if(req.body.gender == 'nam'){
    gender = true;
  }else{
    gender = false;
  }

  models.Student.findOneAndUpdate({username: username_first},{username: username_last,email: email, 
                                  address: address, gender: gender},function(err, user){
      if(err) throw err;
      console.log("display list student");
      res.redirect('home');
  });
  // res.send("hello");
  // console.log(username_first+", "+username_last+", "+email+", "+gender);
});

//xu ly xoa thong tin sinh vien 
router.route('/deleteStd').post(function(req, res){
  var username_dele = req.body.username_dele_hidden;
  models.Student.findOneAndRemove({username: username_dele}, function(err){
    if(err) throw err;
    else{
      res.redirect('home');
    }
  });
  // res.send(username_dele);
});

//router xu ly phan insert
router.route('/searchStd').post(function(req, res){
   
  var username_search = new RegExp(".*"+req.body.name_search+".*");
  // console.log(username_search);
  models.Student.find({username: {$regex: username_search, $options: 'i'}}, function(err, result){
    if (err) throw err;
    else{
      res.render('home', {result_Inf: result, session_name: req.session.user});
      // console.log(result);
    }
  });
});
//router xu ly logout 
router.route('/logout').get(function(req, res){
  req.session.user = null;
  res.redirect('/');
});
module.exports = router;
